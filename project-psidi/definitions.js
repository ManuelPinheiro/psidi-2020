///////////////////////////////////////////////////////
//
// Paulo Gandra de Sousa
// (c) 2016 - 2019
//
///////////////////////////////////////////////////////

/*jslint node: true */
/*jshint esversion:9 */

"use strict";


//
// a documentation file to hold the common definitions of models used in the API
//

/**
 * @swagger
 * components:
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 * 
 */

 /**
  * @swagger
  * components:
  *   schemas:
  *     role:  
  *       description: a role
  *       properties:
  *         name:
  *           type: string  
  */

/**
 * @swagger
 * components:
 *   schemas:
 *     user:
 *       description: a user
 *       required:
 *         - name
 *         - email
 *       properties:
 *         name:
 *           type: string
 *         email:
 *           type: string
 *           format: email
 *         roles:
 *           type: array
 *           items:
 *             id:
 *               $ref: '#/components/schemas/role'
 */


/**
 * @swagger
 * components:
 *   schemas:
 *     users:
 *       description: a collection of users
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/user'
 */


 /**
 * @swagger
 * components:
 *   schemas:
 *     product:
 *       description: a product
 *       required:
 *         - id
 *         - designation
 *         - image_url
 *         - description
 *         - sku 
 *       properties:
 *         id:
 *           type: string
 *         designation:
 *           type: string
 *         image_url:
 *           type: string
 *         description:
 *           type: string
 */


/**
 * @swagger
 * components:
 *   schemas:
 *     products:
 *       description: a collection of products
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/product'
 */

 /**
 * @swagger
 * components:
 *   schemas:
 *     review:
 *       description: a review
 *       required:
 *         - id
 *         - user_id
 *         - review_text
 *         - review_rate
 *         - description
 *         - status
 *         - product_id
 *         - reported
 *         - nr_votes
 *       properties:
 *         id:
 *           type: string
 *         user_id:
 *           type: string
 *         review_text:
 *           type: string
 *         review_rate:
 *           type: number
 *         description:
 *           type: string
 *         status:
 *           type: string
 *           enum: [published, rejected, pending]
 *         product_id:
 *           type: string
 *         date:
 *           type: string
 *         reported:
 *           type: boolean 
 *         nr_votes:
 *           type: integer
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     reviews:
 *       description: a collection of reviews
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/review'
 */