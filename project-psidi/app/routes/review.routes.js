const { authJwt } = require("../middlewares");
const controller = require("../controllers/review.controller");
const { isAdmin } = require("../middlewares/authJwt");

const router = require('express').Router();

router.post('/', [authJwt.verifyToken,authJwt.isCustomer], controller.createReview)

router.get('/pending', [authJwt.verifyToken, authJwt.isModerator], controller.listPending);
router.get('/ofUser',[authJwt.verifyToken,authJwt.isCustomer], controller.getUserReviews);
router.get('/:id/checkStatus', [authJwt.verifyToken, authJwt.isCustomer], controller.checkReviewStatus)
router.get('/reported',[authJwt.verifyToken,authJwt.isModerator],controller.getReportedReviews)
router.get('/:id',controller.getReviewByID)

router.put('/:id/decide', [authJwt.verifyToken, authJwt.isModerator], controller.decideReview)
router.put('/:id/report',[authJwt.verifyToken,authJwt.isCustomer], controller.reportReview)
router.put('/:id/vote',[authJwt.verifyToken,authJwt.isCustomer], controller.voteReview)

router.delete('/:id',[authJwt.verifyToken,authJwt.isCustomer], controller.deleteReview)

module.exports = router;

/**
 * @swagger
 * /api/review/:
 *   post:
 *     tags:
 *     - "review"
 *     summary: Creates a new review.
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       description: Create the new review.
 *       content:
 *         application/json:
 *           schema:
 *            properties:
 *             review_text:
 *               type: string
 *             review_rate:
 *               type: integer
 *             product_id:
 *               type: string
 *           example:
 *             review_text: Good review
 *             review_rate: 4
 *             product_id: 5fd533f151ea9b151c9ba48e
 *     responses:
 *       202:
 *         description: Accepted the review for moderation!!
 *       400:
 *         description: Bad input!
 */

/**
 * @swagger
 * /api/review/pending:
 *    get:
 *     tags:
 *     - "review"
 *     summary: Get review with status pending
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: A list of reviews.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   user_id:
 *                     type: string
 *                   review_text:
 *                     type: string
 *                   review_rate:
 *                     type: integer
 *                   status:
 *                     type: string
 *                     enum: 
 *                       - pending
 *                       - rejected
 *                       - published
 *                   product_id:
 *                     type: string
 *                   date:
 *                     type: string
 *                   reported:
 *                     type: boolean
 */

/**
 * @swagger
 * /api/review/ofUser:
 *    get:
 *     tags:
 *     - "review"
 *     summary: Get review of user
 *     security: 
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: A list of reviews of user.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   user_id:
 *                     type: string
 *                   review_text:
 *                     type: string
 *                   review_rate:
 *                     type: integer
 *                   status:
 *                     type: string
 *                     enum: 
 *                       - pending
 *                       - rejected
 *                       - published
 *                   product_id:
 *                     type: string
 *                   date:
 *                     type: string
 *                   reported:
 *                     type: boolean
 *                   nr_votes:
 *                     type: integer
 */

/**
 * @swagger
 * /api/review/{id}/checkStatus:
 *    get:
 *     tags:
 *     - "review"
 *     summary: Gets review by checkStatus
 *     security:
 *       - bearerAuth: []
 *     operationId: getReviewByStatus
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Status of the review
 *         content:
 *           schema:
 *             type : string
 */
/**
 * @swagger
 * /api/review/reported:
 *    get:
 *     tags:
 *     - "review"
 *     summary: Get every reported review
 *     security:
 *      - bearerAuth: [] 
 *     responses:
 *       '200':
 *         description: A list of reported reviews.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   user_id:
 *                     type: string
 *                   review_text:
 *                     type: string
 *                   review_rate:
 *                     type: integer
 *                   status:
 *                     type: string
 *                     enum: 
 *                       - pending
 *                       - rejected
 *                       - published
 *                   product_id:
 *                     type: string
 *                   date:
 *                     type: string
 *                   reported:
 *                     type: boolean
 *       '403':                   
 *         description: Access to this page is forbidden
 * 
 * 
 */
 /**
 * @swagger
 * /api/review/{id}:
 *   get:
 *     tags:
 *       - review
 *     summary: Get a review by its id
 *     parameters:
 *       - in: path
 *         name: id
 *         description: "Id of the review to be reported"
 *         required: true     
 *     responses:
 *       400:
 *         description: Bad Input.
 *       404:
 *         description: Review with given id was not found.
 *       200:
 *         description: Ok message.
 *         content: 
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/review'
 */


/**
 * @swagger
 * /api/review/{id}/decide:
 *   put:
 *     tags:
 *     - "review"
 *     summary: "Decide wether to publish of reject a review"
 *     security:
 *       - bearerAuth: []
 *     description: ""
 *     operationId: "decideReview"
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *     requestBody:
 *       description: Decide a review.
 *       content:
 *         application/json:
 *           schema:
 *             properties:
 *               update:
 *                 type: string
 *           example:
 *             update: published or rejected
 *     responses:
 *       "200":
 *         description: Review updated
 *       "400":
 *         description: "Invalid ID review"
 *       "404":
 *         description: "Review not found"
 *       "405":
 *         description: "Validation exception"
 */
/**
 * @swagger
 * /api/review/{id}/report:
 *   put:
 *     tags:
 *       - review
 *     summary: Report a review
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: "Id of the review to be reported"
 *         required: true
 *     responses:
 *       200:
 *         description: Ok message, the review was reported.
 *         content: 
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/review'              
 */

/**
 * @swagger
 * /api/review/{id}/vote:
 *   put:
 *     tags:
 *     - "review"
 *     summary: "Update an existing review with increment vote"
 *     security:
 *      - bearerAuth: [] 
 *     description: ""
 *     operationId: "voteReview"
 *     consumes:
 *     - "application/json"
 *     - "application/xml"
 *     produces:
 *     - "application/xml"
 *     - "application/json"
 *     parameters:
 *     - in: path
 *       name: id
 *       description: "Review object that needs to be added to the reviews list"
 *       required: true
 *       schema:
 *         $ref: "#/components/schemas/review"
 *     responses:
 *       "400":
 *         description: "Invalid ID review"
 *       "404":
 *         description: "Review not found"
 *       "405":
 *         description: "Validation exception"
 */
router.put('/:id/vote',[authJwt.verifyToken,authJwt.isCustomer], controller.voteReview)

/**
 * @swagger
 * /api/review/{id}/decideReport:
 *    put:
 *     tags:
 *     - "review"
 *     summary: "Approve or Reject a Reported Review"
 *     security:
 *      - bearerAuth: [] 
 *     description: ""       
 *     parameters:
 *       - in: "path"
 *         name: "id"
 *         description: Review id to decide
 *         required: true
 *         schema: 
 *           type: string
 *       - in: body
 *         name: update
 *         description: The review to create.
 *         schema:
 *           type: string
 *     responses:
 *       "400":
 *         description: "Invalid ID supplied"
 *       "404":
 *         description: "Review not found"
 */

router.put('/:id/decideReport',[authJwt.verifyToken,authJwt.isModerator], controller.decideReport)

/**
 * @swagger
 * /api/review/{id}:
 *    delete:
 *     tags:
 *     - "review"
 *     summary: "Delete a user's review. The user can only delete his reviews. The user can only delete reviews with number of votes = 0"
 *     security:
 *      - bearerAuth: [] 
 *     description: ""
 *     operationId: "deleteReview"
 *     produces:
 *     - "application/xml"
 *     - "application/json"
 *     parameters:
 *     - name: "id"
 *       in: "path"
 *       description: "Review id to delete"
 *       required: true
 *       type: "string"
 *     responses:
 *       "400":
 *         description: "Invalid ID supplied"
 *       "404":
 *         description: "Review not found"
 */

/* module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // Exemplo de catalogo com verificação de token(se o utilizador não tiver feito login ou não ve estes dados)
  app.post("/review", controller.createReview);
  app.get("/getReviewByProduct",controller.getReviewByProduct);
} */