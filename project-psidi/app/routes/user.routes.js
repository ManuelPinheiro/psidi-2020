const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");
const router = require('express').Router()




router.get('/:id', [authJwt.verifyToken] ,controller.getUserByID);
router.put('/:id', controller.update);
//router.delete('/:id', controller.deleteUser);

module.exports = router
/* module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

//Rota para update de utilizador
  app.post("/userUpdate/:id", controller.update);
//Rota para ir buscar um utilizador por id
  app.get("/getUserbyID",  [authJwt.verifyToken], controller.getUserByID);
//Rota para ir buscar todos os utilizadores
  app.get("/allUser", controller.getUsers);
}; */
