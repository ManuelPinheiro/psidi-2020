///////////////////////////////////////////////////////
//
// Paulo Gandra de Sousa
// PSIDI / MEI / ISEP
// (c) 2019
//
///////////////////////////////////////////////////////


/*jslint node: true */
/*jshint esversion:9 */

"use strict";

//
// a node module with utilities
//

const FILTER = ["_id", "__v", "_rev"];

function myOmit(obj, filter) {
    var ret = {};
    var inObj = obj;

    if (obj.toObject) {
        inObj = obj.toObject();
    }

    Object.keys(inObj).forEach(function (k) {
        if (!filter.find(function (e) {
                return e === k;
            })) {
            ret[k] = inObj[k];
        }
    });
    return ret;
}

exports.sendJSON = function (res, code, content, headers) {
    res.status(code);

    // copy headers
    if (headers) {
        Object.keys(headers).forEach(function (h) {
            res.setHeader(h, headers[h]);
        });
    }

    //ensure Etag header
    if (!res.getHeader("ETag")) {
        if (content._rev != null) {
            res.setHeader("ETag", content._rev.toString());
        }
    }

    // filter out implementation properties
    if (Array.isArray(content)) {
        var out = [];
        content.forEach(e => out.push(myOmit(e, FILTER)));
        return res.json(out);
    } else {
        return res.json(myOmit(content, FILTER));
    }
};

exports.sendMsg = exports.sendJSON;

exports.sendTxt = function (res, code, content, headers) {
    res.status(code).type('txt').send(content);
};

exports.etagHeader = function (entry) {
    if (entry && entry.__v != null) {
        return {
            "ETag": entry.__v.toString()
        };
    } else {
        return {};
    }
};

/**
 * 
 * @param {String} url 
 * @param {String} rel 
 */
function linkHeaderElement(url, rel) {
    return "<" + url + ">;rel=" + rel;
}

exports.linkHeaderElement = linkHeaderElement;

/**
 * 
 */
exports.linkHeader = function (data) {
    console.log("data linkHeader"+data)
    var linkstring = "";
    var links = Object.keys(data).map(rel => linkHeaderElement(data[rel], rel));
    for (var i = 0; i <= links.length - 2; i++) {
        linkstring = linkstring.concat(links[i], ",");
    }
    linkstring = linkstring.concat(links[links.length - 1]);
    console.log("Link: "+linkstring);
    return {
        
        "Link": linkstring+';type=ld+json'
    };
};