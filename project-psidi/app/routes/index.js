var express = require('express');
var router = express.Router();

// GET home page. 
router.get('', function(req, res, next) {
   
    let object = { message: "Welcome to the PSIDI api" };

    object._links = [
        { "rel": "projectreview.pt/signup", "href": "http://localhost:4000/api/signup" },
        { "rel": "projectreview.pt/signin", "href": "http://localhost:4000/api/signin" },
        { "rel": "projectreview.pt/products", "href": "http://localhost:4000/api/products" }
    ]
    res.status(200).json(object);
});

router.use('/product' ,require('./product.routes'));
router.use('/review', require('./review.routes'));
router.use('/auth', require('./auth.routes'));
router.use('/user', require('./user.routes'))

module.exports = router;