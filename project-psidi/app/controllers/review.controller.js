const db = require("../models");
var mongoose = require('mongoose');
const url = require('url');
const Product = db.product;
const Review = db.review;
const mongo = require('mongodb')
const ObjectID = mongo.ObjectID
const SERVER_ROOT = "http://localhost:4000/api";

const {
    linkHeader
} = require('../../util');

function createLinksOfReview(param, id) {
    var links;
    if (param == "Report") {
        links = {
            self: "localhost:4000/api/review/" + id + "/report",
            "projectreview.pt/statusReview": "localhost:4000/api/review/" + id + "/checkStatus",
            "projectreview.pt/decideReport": "localhost:4000/api/review/" + id + "/decideReport",
        }
    }

    if (param == "Decide") {
        links = {
            self: "localhost:4000/api/review/" + id + "/decide",
        }
    }

    if (param == "Vote") {
        links = {
            self: "localhost:4000/api/review/" + id + "/vote",
            "projectreview.pt/reportReview": "localhost:4000/api/review/" + id + "/report",
            "projectreview.pt/statusReview": "localhost:4000/api/review/" + id + "/checkStatus",
        }

    }

    if (param == "Status") {
        links = {
            self: "localhost:4000/api/review/" + id + "/checkStatus",
            "projectreview.pt/voteReview": "localhost:4000/api/review/" + id + "/vote",
            "projectreview.pt/reportReview": "localhost:4000/api/review/" + id + "/report",
        }
    }
    return links;
}

function createLinksOfReviewCreate(id) {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/",
        "projectreview.pt/voteReview": "localhost:4000/api/review/" + id + "/vote",
        "projectreview.pt/reportReview": "localhost:4000/api/review/" + id + "/report",
        "projectreview.pt/statusReview": "localhost:4000/api/review/" + id + "/checkStatus",
    };
    return links;
}

function createLinksOfReviewDecideReportUnsuccess(id) {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/" + id + "/decideReport",
    };
    return links;
}

function createLinksOfReviewDecideReportSuccess(id) {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/" + id + "/decideReport",
        "projectreview.pt/voteReview": "localhost:4000/api/review/" + id + "/vote",
        "projectreview.pt/reportReview": "localhost:4000/api/review/" + id + "/report",
        "projectreview.pt/statusReview": "localhost:4000/api/review/" + id + "/checkStatus",
    };
    return links;
}

function buildHref(page, n, product_id) {
    var href = "localhost:4000/api/product/" + product_id + "/review?page=" + page + "&limit=" + n;

    return href;
}

function createLinksOfReviewByProduct(page1, n, product_id) {
    var links;
    var page = parseInt(page1);

    if(page!=1){
        links = {
            // standard IANA rel type
            self: buildHref(page, n, product_id),
            prev: buildHref(page-1, n, product_id),
            next: buildHref(page+1, n, product_id),
        };
    }else{
        links = {
            // standard IANA rel type
            self: buildHref(page, n, product_id),
            next: buildHref(page+1, n, product_id),
        };
    }
    

    return links;
}

function createLinksOfReviewOfUser() {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/ofUser",
    };
    return links;
}

function createLinksOfReviewPending() {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/pending",
    };
    return links;
}

function createLinksOfReviewsReported() {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/review/reported",
    };
    return links;
}


exports.getUserReviews = function (req, res) {

    var userId = req.userId
    const queryObject = url.parse(req.url, true).query;
    var query = { user_id: userId }

    if (queryObject.status)
        query = { user_id: userId, status: queryObject.status }

    Review.find(query)
        .then(result => {
            var headers = linkHeader(createLinksOfReviewOfUser());
            if (headers) {
                Object.keys(headers).forEach(function (h) {
                    res.setHeader(h, headers[h]);
                });
            }

            let object = result;

            object._links = [
                { "rel": "self", "href": "http://localhost:4000/api/review/ofUser" },
            ]

            res.status(200).json(object)
        })
        .catch(error => {
            res.status(500).json({
                message: error.message
            })
        })
}

exports.checkReviewStatus = function (req, res) {

    if (!req.params.id)
        return res.status(500).json("review id required")

    Review.findById(req.params.id)
        .then(result => {
            if (req.userId != result.user_id) {
                return res.status(403).send("You must be the owner of the review to check the status")
            }
            var headers = linkHeader(createLinksOfReview("Status", req.params.id));
            if (headers) {
                Object.keys(headers).forEach(function (h) {
                    res.setHeader(h, headers[h]);
                });
            }

            let object = {message: result.status};

            object._links = [
                { "rel": "self", "href": "http://localhost:4000/api/review/" + req.params.id + "/checkStatus" },
                { "rel": "projectreview.pt/voteReview", "href": "http://localhost:4000/api/review/" + req.params.id + "/vote" },
                { "rel": "projectreview.pt/reportReview", "href": "http://localhost:4000/api/review/" + req.params.id + "/report" },
                { "rel": "projectreview.pt/deleteReview", "href": "http://localhost:4000/api/review/" + req.params.id }
            ]

            res.status(200).json(object)
        })
        .catch(error => {
            res.status(500).json({
                message: error.message
            })
        })
}

function dataFormatada() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = dd + '-' + mm + '-' + yyyy + " at " + today.getHours() + "h : " + today.getMinutes() + "min";
    return today;
}

exports.getReviewByProduct = async (req, res, next) => {
    try {
        
        if(!req.query.page || req.query.page == undefined){
            req.query.page = 1
            req.query.limit = 10
        }
        
        const reviews = await Review.find({ product_id: req.params.id, reported: false, status: 'published' })
            .sort('nr_votes').sort('-date')
            .limit(req.query.limit * 1)
            .skip((req.query.page - 1) * req.query.limit);
        var headers = linkHeader(createLinksOfReviewByProduct(req.query.page, req.query.limit, req.params.id));
        if (headers) {
            Object.keys(headers).forEach(function (h) {
                res.setHeader(h, headers[h]);
            });
        }

        let object = { total: reviews.length, reviews };
        var page = req.query.page;
        var prev = parseInt(page)-1;
        var next = parseInt(page)+1;

        if(req.query.page!=1){
            object._links = [
                { "rel": "self", "href": "http://"+buildHref(req.query.page, req.query.limit, req.params.id) },
                { "rel": "start", "href": "http://"+buildHref(1, req.query.limit, req.params.id) },
                { "rel": "prev", "href": "http://"+buildHref(prev, req.query.limit, req.params.id) },
                { "rel": "next", "href": "http://"+buildHref(next, req.query.limit, req.params.id) }
            ]
        }else{
            object._links = [
                { "rel": "self", "href": "http://"+buildHref(req.query.page, req.query.limit, req.params.id) },
                { "rel": "start", "href": "http://"+buildHref(1, req.query.limit, req.params.id) },
                { "rel": "next", "href": "http://"+buildHref(next, req.query.limit, req.params.id) }
            ]
        }

        res.status(200).json(object);
    } catch (err) {
        res.status(500).json({ error: err });
    }
};

exports.list = function (req, res) {

    const queryObject = url.parse(req.url, true).query;
    var query = {}

    if (queryObject.status)
        query = { status: queryObject.status }


    Review.find(query)
        .then(result => {
            //Função?
            res.status(200).json(result)
        })
        .catch(error => {
            res.status(500).json({
                message: error.message
            })
        })

}

exports.listPending = function (req, res) {

    Review.find({ status: "pending" })
        .then(result => {
            var headers = linkHeader(createLinksOfReviewPending());
            if (headers) {
                Object.keys(headers).forEach(function (h) {
                    res.setHeader(h, headers[h]);
                });
            }

            let object = result;

            object._links = [
                { "rel": "self", "href": "http://localhost:4000/api/review/pending" },
            ]

            res.status(200).json(object)
        })
        .catch(error => {
            res.status(500).json({
                message: error.message
            })
        })
}

exports.createReview = function (req, res) {
    if (!req.userId || !req.body.review_text || !req.body.review_rate || !req.body.product_id) {
        return res.status(400).json("Illformated input. Review text, rate and productId required");
    }
    var newID = new ObjectID();
    //return location

   
    var header = {
        'Location': SERVER_ROOT + "/review/" + newID
    }

    if (header) {
        Object.keys(header).forEach(function (h) {
            res.setHeader(h, header[h]);
        });
    }
    var headers = linkHeader(createLinksOfReviewCreate(newID));

    if (headers) {
        Object.keys(headers).forEach(function (h) {
            res.setHeader(h, headers[h]);
        });
    }

    var result = {
            id: newID,
            message: 'Review accepted for moderation.'
    }

    let object = result;

    object._links = [
        { "rel": "self", "href": "http://localhost:4000/api/review/" },
        { "rel": "projectreview.pt/voteReview", "href": "http://localhost:4000/api/review/" + newID + "/vote" },
        { "rel": "projectreview.pt/reportReview", "href": "http://localhost:4000/api/review/" + newID + "/report" },
        { "rel": "projectreview.pt/statusReview", "href": "http://localhost:4000/api/review/" + newID + "/checkStatus" },
    ]

    res.status(202).json(object)

    /////////////////////
    //create and save the review
    ///////////////////

    const newReview = new Review({
        _id: newID,
        user_id: req.userId,
        product_id: req.body.product_id,
        review_text: req.body.review_text,
        review_rate: req.body.review_rate,
        status: "pending",
        date: dataFormatada()
    });
    console.log
    Product.exists({ _id: req.body.product_id }, (err, result) => {
        if (result == false) return console.log("Product not found")
        Review.create(newReview).then(result => {
            console.log("inserted the following review\n" + result)
        }).catch(err => {
            console.log(err.message)
        })
    })
};

exports.decideReview = function (req, res) {

    var review_id = req.params.id
    var update = { status: req.body.update }

    try {
        Review.exists({ _id: review_id, status: "pending" }).then(result => {
            if (result == false) {
                return res.status(404).send("A pending review with this id was not found")
            }
            Review.findByIdAndUpdate({ _id: review_id }, update, { "new": true }, function (err, model) {
                if (err) throw err
                var headers = linkHeader(createLinksOfReview("Decide", review_id));
                if (headers) {
                    Object.keys(headers).forEach(function (h) {
                        res.setHeader(h, headers[h]);
                    });
                }

                let object = { message: "Review updated", body: model };
                
                object._links = [
                    { "rel": "self", "href": "http://localhost:4000/api/review/"+ review_id + "/decide" }
                ]

                res.status(200).json(object)
            })
        })
    }
    catch (err) {
        res.status(500).json(err.message)
    }
}

exports.decideReport = function (req, res) {

    var review_id = req.params.id
    var update = { status: req.body.update }

    try {
        Review.exists({ _id: review_id, reported: true }).then(result => {
            if (result == false) {
                return res.status(404).send("A reported review with this id was not found")
            }

            if (update.status == "Approved") {
                Review.findByIdAndDelete({ _id: review_id }).then(result => {

                    var headers = linkHeader(createLinksOfReviewDecideReportUnsuccess(review_id));
                    if (headers) {
                        Object.keys(headers).forEach(function (h) {
                            res.setHeader(h, headers[h]);
                        });
                    }

                    let object = { message: "Report Approve and Review Deleted", body: result };

                    object._links = [
                        { "rel": "self", "href": "http://localhost:4000/api/review/"+ review_id + "/decideReport" }
                    ]

                    res.status(200).json(object)
                })

            } else {
                Review.findByIdAndUpdate({ _id: review_id }, { reported: false }, { "new": true }, function (err, model) {
                    if (err) throw err
                    var headers = linkHeader(createLinksOfReviewDecideReportSuccess(review_id));
                    if (headers) {
                        Object.keys(headers).forEach(function (h) {
                            res.setHeader(h, headers[h]);
                        });
                    }

                    let object = { message: "Review updated", body: model };

                    object._links = [
                        { "rel": "self", "href": "http://localhost:4000/api/review/"+ review_id + "/decideReport" }
                    ]


                    res.status(200).json(object)
                })
            }

        })
    }
    catch (err) {
        res.status(500).json(err.message)
    }
}

//product/{id}/totalRating
exports.getAgregatedRating = function (req, res) {
    if (!req.params.id) {
        return res.status(400).send("A product id is needed")
    }
    try {
        Review.exists({ product_id: req.params.id }).then(result => {
            if (result == false) {
                return res.status(404).send("Reviews not found. This product has no reviews")
            }
            Review.find({ product_id: req.params.id }, { review_rate: 1, _id: 0 }).then(result => {

                var agregado = 0;
                var numero = 0;
                result.forEach(element => {
                    numero++;
                    agregado += element.review_rate;
                });

                //console.log("Agregado %d total %d rating %d",agregado,numero,(agregado/numero))
                return res.status(200).json({ totalRating: (agregado / numero) })
            })
        })
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

//review/reported
exports.getReportedReviews = function (req, res) {
    Review.find({ reported: true }).then(result => {

        var headers = linkHeader(createLinksOfReviewsReported());
                if (headers) {
                    Object.keys(headers).forEach(function (h) {
                        res.setHeader(h, headers[h]);
                    });
                }

        let object = result;

        object._links = [
            { "rel": "self", "href": "http://localhost:4000/api/review/reported" }
        ]

        res.status(200).json(object)
    })
        .catch(error => {
            res.status(500).json({
                message: error.message
            })
        })
}

exports.reportReview = function (req, res) {
    var review_id = req.params.id
    var update = { reported: true }

    try {
        Review.exists({ _id: review_id }).then(result => {
            if (result == false) {
                return res.status(404).send("A review with this id was not found")
            }
            Review.findByIdAndUpdate({ _id: review_id }, update, { "new": true }, function (err, model) {
                if (err) throw err
                var headers = linkHeader(createLinksOfReview("Report", review_id));
                if (headers) {
                    Object.keys(headers).forEach(function (h) {
                        res.setHeader(h, headers[h]);
                    });
                }

                let object = model;

                object._links = [
                    { "rel": "self", "href": "http://localhost:4000/api/review/"+ review_id + "/report" }
                ]


                res.status(200).json(object)
            })
        })
    }
    catch (err) {
        res.status(500).json(err.message)
    }
}

exports.voteReview = async function (req, res) {

    Review.update({ _id: req.params.id }, { $inc: { nr_votes: 1 } }).then(result => {
        var headers = linkHeader(createLinksOfReview("Vote", req.params.id));
        if (headers) {
            Object.keys(headers).forEach(function (h) {
                res.setHeader(h, headers[h]);
            });
        }

        let object = { message: "You voted this review!!" };

        object._links = [
            { "rel": "self", "href": "http://localhost:4000/api/review/"+ req.params.id + "/vote" },
            { "rel": "projectreview.pt/reportReview", "href": "http://localhost:4000/api/review/"+ req.params.id + "/report" }
        ]


        res.status(200).json(object)
    }).catch(error => {
        res.status(500).json({
            message: error.message
        })
    })

}


exports.deleteReview = async function (req, res) {

    if (!req.params.id) return res.status(400).json({ message: "Bad input!" })
    Review.findOne({ _id: req.params.id }).then(result => {

        if (!result) {
            return res.status(404).json({ message: "Review not found!" })
        }
        if (result.user_id != req.userId) {
            return res.status(403).json({ message: "You can only delete your reviews!" })
        }
        if (result.nr_votes > 0) {
            return res.status(405).json({ message: 'Review cannot be deleted! Number of votes > 0' })
        }

        result.delete().then(result => {
            console.log(result)
            res.status(200).json({ message: 'Review successfully deleted' })
        })

    }).catch(err => {
        res.status(500).json({ message: err.message })
    })

};

/*
exports.updateReportedReview = function (req,res){
    var review_id = req.params.id
    
    var update = { reported: req.body.decision }

    try {
        Review.exists({ _id: review_id}).then(result => {
            if (result == false) {
                return res.status(404).send("A review with this id was not found")
            }
            Review.findByIdAndUpdate({ _id: review_id }, update, { "new": true }, function(err, model){
                if(err) throw err
                res.status(200).json({message: "Review updated", body: model})
            })
        })
    }
    catch (err) {
        res.status(500).json(err.message)
    }
}*/

exports.getReviewByID = function (req, res) {
    console.log(req.params.id)
    if (!req.params.id) return res.status(400).json({ message: "Bad input" })
    Review.find({_id:req.params.id, reported: false}).then(result => {
        if (result == null || result == []) return res.status(404).json({ message: "Review not found" })
        return res.status(200).json(result)
    })
}
