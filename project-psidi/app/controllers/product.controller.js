const db = require("../models");
var mongoose = require('mongoose');
const Product = db.product;

const {
    linkHeader
  } = require('../../util');

  function buildHref(page, n) {
    var href = "localhost:4000/api/product?page=" + page + "&limit=" + n;

    return href;
}


function createLinksOfProduct(product) {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/product/",
        item: "localhost:4000/api/product/" + product.id,
    };
    return links;
  }

  function createLinksOfProductByID(product) {
    var links = {
        // standard IANA rel type
        self: "localhost:4000/api/product/"+ product.id,
        "projectreviews.pt/reviewsOfProduct": "localhost:4000/api/product/"+ product.id+"/reviews?page=1&limit=10" ,
        "projectreview.pt/addReview": "localhost:4000/api/review"
    };
    return links;
  }

  function createLinksOfProductCollection(req) {
 
    var links;

    if(req.query.page==null){
        var page = parseInt(req.query.page);
        if(page==1){
            links = {
                self: buildHref(page,req.query.limit),
                start: buildHref(1,re.query.limit),
                next: buildHref(page+1,re.query.limit),
            };
        }else{
            links = {
                self: buildHref(page,req.query.limit),
                start: buildHref(1,re.query.limit),
                prev: buildHref(page-1,re.query.limit),
                next: buildHref(page+1,re.query.limit),
            }; 
        }
        
    }else{
        links = {
            self: buildHref(1,10),
            start: buildHref(1,10),
            next: buildHref(2,10),
        };
    }
    
    return links;
  }

//criar uma catalogue Apenas um exemplo
exports.createProduct = function (req, res) {

    if(!req.body.description || !req.body.designation || !req.body.sku || !req.file){
        return res.status(400).json({message: "Description, desigantion, sku and file are needed"})
    }
    if(req.body.sku < 10000000 || req.body.sku > 99999999){
        return res.status(400).json({message: "sku has to be between 10000000 and 99999999"})
    }
    const newProduct = new Product({
        description: req.body.description,
        designation: req.body.designation,
        sku: req.body.sku,
        image_url: req.file.path
    });
    console.log(newProduct)
    newProduct.save(function (err, product) {
        if (err) return res.status(500).send({ message: err.message });
        //Add header
        console.log(product.id);
        var headers = linkHeader(createLinksOfProduct(product));
            if (headers) {
              Object.keys(headers).forEach(function (h) {
                  res.setHeader(h, headers[h]); 
              });
            }

            let object = { message: "The product was created!!", product: product }

            object._links = [
                { "rel": "self", "href": "http://localhost:4000/api/product/" },
                { "rel": "item", "href": "http://localhost:4000/api/product/"+ product.id +"/reviews?page=1&limit=10" }
            ]


        res.status(201).json(object);
    });
}

//Get de um utilizador por ID
exports.getProductByID = (req, res) => {

    Product.findById(req.params.id, function (err, product) {
        if (err) res.status(301).send({ message: err });
        //Add header
        var headers = linkHeader(createLinksOfProductByID(product));
            if (headers) {
              Object.keys(headers).forEach(function (h) {
                  res.setHeader(h, headers[h]); 
              });
            }

        let object = { message: product };
        object._links = [
            { "rel": "self", "href": "http://localhost:4000/api/product/"+ req.params.id },
            { "rel": "projectreviews.pt/reviewsOfProduct", "href": "http://localhost:4000/api/product/"+ req.params.id +"/reviews?page=1&limit=10" },
            { "rel": "projectreview.pt/addReview", "href": "http://localhost:4000/api/review" }
        ]

        res.status(200).json(object);
    });
};

//Get all Products
exports.allProducts = async (req, res, next) => {
    try {
        const { page = 1, limit = 10 } = req.query;
        const products = await Product.find()
            .limit(limit * 1)
            .skip((page - 1) * limit);
            //Add header
        var headers = linkHeader(createLinksOfProductCollection(req));
        if (headers) {
          Object.keys(headers).forEach(function (h) {
              res.setHeader(h, headers[h]); 
          });
        }
        let object = { total: products.length, products };
        

        if(req.query.page!=null){
            var page1 = parseInt(req.query.page);
            if(page1!=1){
                object._links = [
                    { "rel": "self", "href": "http://"+buildHref(page1, req.query.limit) },
                    { "rel": "start", "href": "http://"+buildHref(1, req.query.limit) },
                    { "rel": "prev", "href": "http://"+buildHref(page1-1, req.query.limit) },
                    { "rel": "next", "href": "http://"+buildHref(page1+1, req.query.limit) }
                ]
            }else{
                object._links = [
                    { "rel": "self", "href": "http://"+buildHref(page1, req.query.limit) },
                    { "rel": "start", "href": "http://"+buildHref(1, req.query.limit) },
                    { "rel": "next", "href": "http://"+buildHref(page1+1, req.query.limit) }
                ]
            }
            
        }else{
            object._links = [
                { "rel": "self", "href": "http://"+buildHref(1, 10) },
                { "rel": "start", "href": "http://"+buildHref(1, 10) },
                { "rel": "next", "href": "http://"+buildHref(2, 10) }
            ]
        }
        
        res.status(200).json(object);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error });
    }
};