const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");
const router = require('express').Router()



// Routes

/**
* @swagger
* /api/auth/signup:
*   post:
*     tags:
*       - auth
*     summary: Creates a new user.
*     requestBody:
*       content:
*         application/json:
*           schema:
*             type: object
*             properties:
*               username:
*                 type: string
*               email:
*                 type: string
*               password:
*                 type: string
*             example:
*               username: customer390
*               email: customer390@email.com
*               password: '122334'
*     responses:
*       '201':
*         description: The user was created !!
*       '400':
*         description: Bad input!
 */
router.post('/signup', [
  verifySignUp.checkDuplicateUsernameOrEmail,
  verifySignUp.checkRolesExisted
], 
controller.signup)


/**
 * @swagger
 * /api/auth/signin:
 *  post:
 *     tags:
 *     - "auth"
 *     summary: Log into the application.
 *     consumes:
 *       - application/json
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:      
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *             example:   
 *               email: customer284@email.com
 *               password: "123123123"
 *     responses:
 *       201:
 *         description: The user was logged in !!
 *       404:
 *         description: User not found
 */
router.post('/signin', controller.signin)

module.exports = router;

/* module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  //Rota para fazer Registo de utilizador
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUsernameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    controller.signup
  );
  
  //Rota para fazer Login de utilizador
  app.post("/api/auth/signin", controller.signin);

}; */
