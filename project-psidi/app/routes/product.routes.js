const { authJwt } = require("../middlewares");
const controller = require("../controllers/product.controller");
const controller1 = require("../controllers/review.controller");
const { isAdmin } = require("../middlewares/authJwt");
const router = require('express').Router();

var multer = require('multer');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads-img');
    },
    filename: function(req, file, cb) {
      cb(null, file.originalname);
    }
  });
  
  
  const upload = multer({
    storage: storage
  });

/**
 * @swagger
 * 
 *paths:
 *  /api:
 *    summary: Welcoming page of the aplication
 *    get:
 *      summary: Displays a welcoming message to the application
 *      description: ''
 *      operationId: ''
 *      responses:
 *        200:
 *          description: Ok message
 *  /api/product/:
 *   get:
 *     tags:
 *     - "product"
 *     summary: Get all products
 *     parameters:
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *         description: The page number. Each page contains the limited amount of products
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *         description: The numbers of items to return
 *     responses:
 *      200:
 *         description: List of products
 *   
 *  /api/product/{id}:
 *    get:
 *     tags:
 *     - "product"
 *     description: Returns product by ID
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *      200:
 *         description: Return the product associated with the given ID.
 *      400:
 *         description: Bad input!
 *      404: 
 *         description: Product with given id was not found
 *      500:
 *         description: Unexpected error.
 */

router.get('/:id',upload.single('image_url'), controller.getProductByID)
/**
 * @swagger
 * /api/product/{id}/reviews:
 *   get:
 *     tags:
 *     - "product"
 *     summary: Get the reviews of a given product (product id)
 *     operationId: getReviewProduct
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *       - in: query
 *         name: page
 *         required: true
 *         schema: 
 *           type: string
 *       - in: query
 *         name: limit
 *         required: true
 *         schema: 
 *           type: string
 *     responses:
 *       '200':
 *         description: A collection of the product's reviews.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/reviews'
 *       400: 
 *         description: Bad input!
 *       404:
 *         description: Not found!
 *       500:
 *         description: Unexpected server error. 
 */
router.get('/:id/reviews', controller1.getReviewByProduct)

 router.post('/',upload.single('image_url'),[authJwt.verifyToken,authJwt.isAdmin],controller.createProduct)
// router.post('/',upload.single('image_url'),controller.createProduct)

/**
 * @swagger
 * '/api/product/{id}/totalRating':
 *   summary: Gets the total rating of a product
 *   get:
 *     tags:
 *       - "product"
 *     summary: Get the total rating of a product by ID
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: the message id
 *     responses:
 *       200:
 *         description: Default sucess sample response
 *         content:
 *           application/json:
 *             examples:
 *               OK response:
 *                 value: '{"totalRating": 4.5}'
 */
router.get('/:id/totalRating',controller1.getAgregatedRating)


router.get('/',controller.allProducts)

/**
 * @swagger
 * /api/product/:
 *    post:
 *      summary: Creates a new product.
 *      security: 
 *        - bearerAuth: []
 *      consumes:
 *        - "multipart/form-data"
 *      tags:
 *        - "product"
 *      requestBody:
 *        content: 
 *          multipart/form-data: # Media type
 *            schema:            # Request payload
 *              type: object
 *              properties: 
 *                designation:
 *                  type: string     # Request parts
 *                description:
 *                  type: string
 *                sku:
 *                  type: integer
 *                  minimum: 10000000
 *                  maximum: 99999999
 *                image_url:  # Part 3 (an image)
 *                  description: An image to upload
 *                  type: string
 *                  format: binary
 *      responses:
 *        201:
 *          description: Product created successfully!
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  message:
 *                    type: string
 *                  product:
 *                    $ref: '#/components/schemas/product'
 *        400:
 *          description: Bad input. Something is missing.
 *        401:
 *          description: Unauthorized. No login tokin was found.
 *        403:
 *          description: Forbidden. You don't have permission to acess this resource.
 *        '5XX':
 *          description: Unexpected error.
 */
module.exports = router;
/* module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // Exemplo de product sem verificação de token(se o utilizador tiver feito login ou não ve sempre estes dados)
  //app.get("/product", controller.allCatalogue);

  // Exemplo de catalogo com verificação de token(se o utilizador não tiver feito login ou não ve estes dados)
  app.post("/product",[authJwt.verifyToken,authJwt.isAdmin], controller.createProduct);
  app.get("/getProductDetail",controller.getProductByID);

} */

