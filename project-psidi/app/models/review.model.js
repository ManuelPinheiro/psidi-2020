const mongoose = require("mongoose");

const Review = mongoose.model(
  "Review",
  new mongoose.Schema({
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
      },
    review_text:{type: String, required: true},
    review_rate:{type:Number,default:0, min:0,max:5},
    status:{
      type: String,
      enum: ['published', 'rejected', 'pending'],
      required: true,
      default: 'pending'
    },
    product_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: "product"
    },
    date:{ type: String, require: false},
    nr_votes:{type: Number , require:false, default:0},
    reported:{
      type: Boolean,
      required: true,
      default: false
    }
  })
);

module.exports = Review;
