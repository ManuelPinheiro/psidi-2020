var mongoose = require('mongoose');
const User = mongoose.model('User')
var bcrypt = require("bcryptjs");
const nodemailer = require('nodemailer');
const fs = require('fs');
var handlebars = require('handlebars');



//Atualizar um Utilizador
exports.update = (req, res) => {

 var id = mongoose.Types.ObjectId(req.query.id);


  const user = {
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  }

  User.findOneAndUpdate({_id:id},user, { new: true }, function (err, user) {
    if (err) res.status(301).send({ message: err})

    res.status(200).send({ message: user });
  });

};

//Get de um utilizador por ID
exports.getUserByID = (req, res) => {

  var id = mongoose.Types.ObjectId(req.query.id);

  User.findById(mongoose.Types.ObjectId(id), function (err, user) {
    if (err) res.status(301).send({ message: err });
    res.status(200).send({ message: user });
  });
};

//Get de todos os utilizador
exports.getUsers = (req, res) => {

  User.find({}, function (err, user) {
    if (err) res.send(err)
    res.status(200).send({ message: user });
  });
};

