const mongoose = require("mongoose");

// const m2s = require('mongoose-to-swagger');

const Product = mongoose.model(
  "Product",
  new mongoose.Schema({
    designation:{type: String, required: true},
    description:{type: String, required: true},
    sku:{type:String, required:true, match:/^\d{8}$/},
    image_url:{type:String}
  })
);

// const swagger=m2s(Product);
// console.log(swagger);

module.exports = Product;