const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;

var mongoose = require('mongoose');
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

const {
  linkHeader
} = require('../../util');

function createLinksOfSignIn(authorities) {
  // collection links
  var links = {
      self : "localhost:4000/api/signin",
  };

  var linksmod = "";
  var linksadm = "";
  var linkscost = "";

  for(var i = 0;i<authorities.length;i++){
    if(authorities[i]=="ROLE_ADMIN"){
      linksadm = {
        "projectreview.pt/addProduct" : "localhost:4000/api/product",
      };
    }
    if(authorities[i]=="ROLE_MODERATOR"){
      linksmod = {
        "projectreview.pt/pendingReviews" : "localhost:4000/api/review/pending",
        "projectreview.pt/reportedReviews" : "localhost:4000/api/review/reported",
      };
    }
    if(authorities[i]=="ROLE_CUSTOMER"){
      linkscost = {
        "projectreview.pt/products" : "localhost:4000/api/products?page=1&limit=10",
        "projectreview.pt/reviewsOfUser" : "localhost:4000/api/reviews/ofUser",
      };
    } 

  }

  links += linksadm + linksmod + linkscost;

  return links;
}

function createLinksOfSignUp() {
  // collection links
  var links = {
      self : "localhost:4000/api/signup",
      "projectreview.pt/signin" : "localhost:4000/api/signin",
  };
  return links;
}

//Registo de utilizador
exports.signup = (req, res) => {
  console.log("LIUSFEBLSIUFBSIOUÇFB\n"+req.body.username+req.body.email+req.body.password)
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  });


  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role._id);
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            var headers = linkHeader(createLinksOfSignUp());
            if (headers) {
              Object.keys(headers).forEach(function (h) {
                  res.setHeader(h, headers[h]); 
              });
            }

            let object = { message: "User was registered successfully! 1" };

            object._links = [
              { "rel": "self", "href": "http://localhost:4000/api/signup" },
              { "rel": "sprojectreview.pt/signin", "href": "http://localhost:4000/api/signin" }
          ]

            res.status(201).json(object);
          });
        }
      );
    } else {
      Role.findOne({ name: "customer" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save(err => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          var headers = linkHeader(createLinksOfSignUp());
          if (headers) {
            Object.keys(headers).forEach(function (h) {
                res.setHeader(h, headers[h]);
               
            });
          }
          let object = { message: "User was registered successfully! 2" };

            object._links = [
              { "rel": "self", "href": "http://localhost:4000/api/signup" },
              { "rel": "sprojectreview.pt/signin", "href": "http://localhost:4000/api/signin" }
          ]

            res.status(201).json(object);
         });
      });
    }
  });
};

//Login de Utilizador
exports.signin = (req, res) => {
  console.log(req.body.email)
  User.findOne({
    email: req.body.email
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }


      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      var headers = linkHeader(createLinksOfSignIn(authorities));
          if (headers) {
            Object.keys(headers).forEach(function (h) {
                res.setHeader(h, headers[h]);
               
            });
          }

          let object = {
            id: user._id,
            username: user.username,
            email: user.email,
            roles: authorities,
            accessToken: token
          }

          object._links = [
            { "rel": "self", "href": "http://localhost:4000/api/signin" },
        ]

        for(var i = 0;i<authorities.length;i++){
          if(authorities[i]=="ROLE_ADMIN"){
            object._linksadm = [
              { "rel": "projectreview.pt/addProduct", "href" : "http://localhost:4000/api/product"}
            ]    
          }
          if(authorities[i]=="ROLE_MODERATOR"){
            object._linksmod = [
              { "rel": "projectreview.pt/pendingReviews", "href" : "http://localhost:4000/api/review/pending"},
              { "rel": "projectreview.pt/reportedReviews", "href" : "http://localhost:4000/api/review/reported"}
            ]
          }
          if(authorities[i]=="ROLE_CUSTOMER"){
            object._linkscost = [
              { "rel": "projectreview.pt/products", "href" : "http://localhost:4000/api/products?page=1&limit=10"},
              { "rel": "projectreview.pt/reviewsOfUser", "href" : "http://localhost:4000/api/reviews/ofUser"}
            ]
          } 
      
        }



      res.status(200).json(object);
    });
};
