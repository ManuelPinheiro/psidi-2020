const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

//Todos os models
db.user = require("./user.model");
db.role = require("./role.model");
db.product = require("./product.model");
db.review = require("./review.model");
db.ROLES = ["admin", "moderator","customer"];

module.exports = db;