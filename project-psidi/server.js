const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./app/config/db.config");
const morgan = require('morgan');
const app = express();
const fs = require('fs');

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");


const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      version: "1.0.1",
      title: "Psidi Review Api",
      description: "A level 3 Rest API of reviews",
      contact: {
        name: "Grupo303"
      },
    },
    servers: [{
      url: "http://localhost:4000"
    }],
  },
  // ['.routes/*.js']
  //"./definitions.js",
  apis:["./definitions.js","./app/routes/*.js",]

};




const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
fs.writeFileSync("./swagger-spec.json", JSON.stringify(swaggerDocs));





app.use(cors());
app.use(morgan('dev'));
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
const Role = db.role;

//Config da mongo DB presente em app/config/db.config.js
db.mongoose
  //.connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
  .connect(`mongodb+srv://admin:psidi@psidi-database.apsdt.mongodb.net/psidi?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false 
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

app.use('/api', require('./app/routes'));

// set port, listen for requests
const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});


//Dinâmica que corre pela primeira vez que se corre a app
function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });

      new Role({
        name: "customer"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'customer' to roles collection");
      });
    }
  });
}
